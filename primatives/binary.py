from ..state import State
import shutil
import os


class BinaryContentState(State):
    """Ensure a file matches given binary content."""

    def apply(self, context):
        if self.should_run(context):
            try:
                self._write_content(self.content, self.destination)
            except AttributeError as e:
                context.out('missing mandatory attribute on template state:')
                context.out(e)
                return False
            pass
        else:
            context.out(' * Skipping state, should_run returned falsey.')
        return True

    def _write_content(self, content, destination):
        backup = '{}.bak'.format(destination)

        try:
            shutil.move(destination, backup)
        except FileNotFoundError:
            pass

        with open(destination, 'wb') as file:
            file.truncate()
            file.write(content)

        try:
            os.remove(backup)
        except FileNotFoundError:
            pass

        return True
