from sys import stdin, stdout, stderr
from ..state import State
import subprocess


class CommandState(State):
    """Run a command via subprocess execution."""

    def apply(self, context):
        try:
            transparent = getattr(self, 'transparent', True)

            out_code = subprocess.call(self.command,
                                       stdin=stdin if transparent else None,
                                       stdout=stdout if transparent else None,
                                       stderr=stderr if transparent else None)

            return out_code == 0
        except AttributeError as e:
            context.out('missing mandatory attribute on command state:')
            context.out(e)
            return False
