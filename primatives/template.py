from ..state import State
import subprocess
import sys
import shutil
import os


class TemplateState(State):
    """Ensure a file matches a given template"""

    def apply(self, context):
        if self.should_run(context):
            try:
                self._write_template(
                    self.template, self.destination, self.args)
            except AttributeError as e:
                context.out('missing mandatory attribute on template state:')
                context.out(e)
                return False
            pass
        else:
            context.out(' * Skipping state, should_run returned falsey.')
        return True

    def _write_template(self, template, destination, args):
        backup = '{}.bak'.format(destination)

        try:
            shutil.move(destination, backup)
        except FileNotFoundError:
            pass

        with open(destination, 'w') as file:
            file.truncate()
            file.write(template.format(**args))

        try:
            os.remove(backup)
        except FileNotFoundError:
            pass

        return True
