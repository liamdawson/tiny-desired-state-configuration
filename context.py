from __future__ import print_function


class DesiredStateContext:
    tags = set()

    # TODO: change out_file to logger of some kind
    def __init__(self, out_file):
        self.out_file = out_file

    def out(self, *args, **kwargs):
        print(*args, file=self.out_file, **kwargs)
