class TagExtractor:
    extractions = dict()

    def __init__(self, extractions):
        self.extractions = extractions

    def extract(self, input_tags):
        unextracted = set(input_tags)
        extracted = self._extract_set(unextracted)

        while not extracted == unextracted:
            difference = extracted.difference(unextracted)
            unextracted = extracted
            extracted = extracted.union(self._extract_set(difference))

        return extracted

    def _extract_set(self, tags):
        out_tags = set(tags)

        for key in self.extractions:
            for value in self.extractions.get(key):
                if value in tags:
                    out_tags.add(key)

        return out_tags

