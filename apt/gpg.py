from ..primatives import BinaryContentState


class EnsureAptGpgKey(BinaryContentState):
    """Ensure a given GPG key is available to apt."""
    tags = [set(['ubuntu']), set(['debian'])]

    @property
    def key_name(self):
        raise NotImplementedError(
            'key_name is required to choose gpg key name')

    @property
    def key(self):
        raise NotImplementedError(
            'key content is required to set gpg key content')

    @property
    def content(self):
        return self.key

    @property
    def destination(self):
        return '/etc/apt/trusted.gpg.d/{}.gpg'.format(self.key_name)
