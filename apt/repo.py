from ..primatives import TemplateState


class EnsureAptRepositoryEnabled(TemplateState):
    """Ensure a given apt repository is enabled."""
    tags = [set(['ubuntu']), set(['debian'])]
    template = "deb {arch} {url} {version} {repos}"

    @property
    def repo_name(self):
        raise NotImplementedError(
            'repo_name is required to choose repository file name')

    @property
    def repo_arch(self):
        return None

    @property
    def repo_url(self):
        raise NotImplementedError('repo_url is required')

    @property
    def repo_version(self):
        raise NotImplementedError('repo_version is required')

    @property
    def repo_repos(self):
        raise NotImplementedError('repo_repos is required')

    @property
    def args(self):
        return {
            'arch': '[arch={}]'.format(self.repo_arch) if self.repo_arch else '',
            'url': self.repo_url,
            'version': self.repo_version,
            'repos': ' '.join(self.repo_repos)
        }

    @property
    def destination(self):
        return '/etc/apt/sources.list.d/{}.list'.format(self.repo_name)
