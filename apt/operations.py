from ..state import State
from sys import stdin, stdout, stderr
import subprocess


class UpdateAptPackageList(State):
    """Update apt package list."""
    name = "apt-get update"
    tags = [set(['ubuntu']), set(['debian'])]

    def apply(self, _):
        out_code = subprocess.call(
            ['apt-get', 'update', '-y'], stdin=stdin, stdout=stdout, stderr=stderr)

        return out_code == 0


class UpdateAptPackages(State):
    """Update installed packages via apt."""
    name = "apt-get upgrade"
    tags = [set(['ubuntu']), set(['debian'])]

    def apply(self, _):
        out_code = subprocess.call(
            ['apt-get', 'upgrade', '-y'], stdin=stdin, stdout=stdout, stderr=stderr)

        return out_code == 0
