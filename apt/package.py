from ..state import State
from sys import stdin, stdout, stderr
import subprocess
import re


class InstallAptPackages(State):
    """Ensure the given apt packages are installed."""
    tags = [set(['ubuntu']), set(['debian'])]
    packages = []

    def apply(self, _):
        out_code = subprocess.call(
            ['apt-get', 'install', '-y'] + self.packages, stdin=stdin, stdout=stdout, stderr=stderr)

        return out_code == 0


class RemoveAptPackages(State):
    """Ensure the given apt packages are not installed."""
    tags = [set(['ubuntu']), set(['debian'])]
    packages = []

    def _filter_installed_packages(self, packages):
        package_list = subprocess.check_output(
            ['dpkg', '--get-selections'] + packages).decode('utf-8')
        exp = re.compile(r"(.*?)\s+install")
        installed = set([exp.sub(r"\1", line)
                         for line in package_list.splitlines()])

        return [package for package in packages if package in installed]

    def should_run(self, _):
        return any(self._filter_installed_packages(self.packages))

    def apply(self, _):
        current_packages = self._filter_installed_packages(self.packages)

        out_code = subprocess.call(
            ['apt-get', 'remove', '-y'] + current_packages, stdin=stdin, stdout=stdout, stderr=stderr)

        return out_code == 0
