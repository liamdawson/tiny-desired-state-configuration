from ..primatives import CommandState


class SetGsettingState(CommandState):
    """Set a gsettings value value"""
    transparent = True

    @property
    def command(self):
        return ['gsettings', 'set', self.namespace, self.setting, self.value]

    @property
    def namespace(self):
        raise NotImplementedError('required property namespace not set')

    @property
    def setting(self):
        raise NotImplementedError('required property setting not set')

    @property
    def value(self):
        raise NotImplementedError('required property value not set')
