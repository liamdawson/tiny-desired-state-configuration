class State:
    def should_apply(self, context):
        try:
            return any([context.tags.issuperset(the_set) for the_set in self.tags]) and self.should_run(context)
        except AttributeError:
            return False

    def should_run(self, context):
        """Return custom logic to indicate if the state should be run."""
        return True

    def apply(self, context):
        raise NotImplementedError(
            "Apply not implemented for {}".format(self.name))

    @property
    def name(self):
        return self.__class__.__name__

    @property
    def description(self):
        return self.__class__.__doc__
