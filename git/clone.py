from ..state import State
from sys import stdin, stdout, stderr
import subprocess
import os

class CloneIfMissingState(State):
    clone_flags = []

    def should_run(self, context):
        return not os.path.exists(self.destination)

    def apply(self, context):
        out_code = subprocess.call(['git', 'clone', self.source, self.destination] + self.clone_flags, stdin=stdin, stdout=stdout, stderr=stderr)
        return out_code == 0

    @property
    def source(self):
        raise NotImplementedError('missing source for clone state')

    @property
    def destination(self):
        raise NotImplementedError('missing destination for clone state')
