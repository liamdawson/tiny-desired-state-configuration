class TagExpander:
    expansions = dict()

    def __init__(self, expansions):
        self.expansions = expansions

    def expand(self, input_tags):
        unexpanded = set(input_tags)
        expanded = self._expand_set(unexpanded)

        while not expanded == unexpanded:
            difference = expanded.difference(unexpanded)
            unexpanded = expanded
            expanded = expanded.union(self._expand_set(difference))

        return expanded

    def _expand_set(self, tags):
        out_tags = set(tags)

        for tag in tags:
            expansions = self.expansions.get(tag, None)

            if expansions:
                out_tags = out_tags.union(set(expansions))

        return out_tags

