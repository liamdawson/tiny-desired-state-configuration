from unittest import TestCase
from tag_expander import TagExpander


class TagExpanderTestSuite(TestCase):
    def test_no_change_when_no_aliases_match(self):
        expanded = TagExpander({
            'foo': ['bar']
        }).expand(['baz'])
        self.assertSetEqual(expanded, {'baz'})

    def test_aliases_expand_recursively(self):
        expanded = TagExpander({
            'foo': ['bar'],
            'bar': ['baz']
        }).expand(['foo'])
        self.assertSetEqual(expanded, {'foo', 'bar', 'baz'})

    def test_aliases_expand_multiple(self):
        expanded = TagExpander({
            'foo': ['bar', 'baz'],
            'qux': ['quz']
        }).expand(['foo', 'qux'])
        self.assertSetEqual(expanded, {'foo', 'bar', 'baz', 'qux', 'quz'})
