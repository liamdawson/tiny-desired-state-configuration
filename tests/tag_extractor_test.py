from unittest import TestCase
from tag_extractor import TagExtractor


class TagExtractorTestSuite(TestCase):
    def test_no_change_when_no_aliases_match(self):
        extracted = TagExtractor({
            'foo': ['bar']
        }).extract(['baz'])
        self.assertSetEqual(extracted, {'baz'})

    def test_aliases_extract_recursively(self):
        extracted = TagExtractor({
            'linux': ['debian'],
            'debian': ['ubuntu']
        }).extract(['ubuntu'])
        self.assertSetEqual(extracted, {'linux', 'debian', 'ubuntu'})

    def test_handles_multiple_matches(self):
        extracted = TagExtractor({
            'sh': ['bash', 'zsh'],
            'bash': ['linux'],
            'zsh': ['linux'],
            'fish': ['linux']
        }).extract(['linux'])
        self.assertSetEqual(extracted, {'sh', 'bash', 'zsh', 'fish', 'linux'})
