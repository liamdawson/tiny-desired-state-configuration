from reducer import DesiredStateReducer
from state import State
import unittest


class IrrelevantState(State):
    id = 1

    def should_apply(self, _):
        return False


class RelevantState(State):
    id = 2

    def should_apply(self, _):
        return True


class RelevantStateThatShouldNotRun(RelevantState):
    id = 3

    def should_run(self, _):
        return False

    def apply(self, context):
        raise AssertionError('{} should not be applied.'.format(self.__class__.__name__))


class ReducerTest(unittest.TestCase):

    def test_should_apply_filtering(self):
        reducer = DesiredStateReducer([IrrelevantState(), RelevantState(), RelevantStateThatShouldNotRun()])

        states = [state.id for state in reducer.filter(None)]

        self.assertListEqual(states, [2, 3])
