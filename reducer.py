class DesiredStateReducer:
    """Determines the set of relevant states based on the context.

    Handles logic for filtering a list of states down to a list relevant to the
    given DesiredStateContext.
    """

    def __init__(self, states):
        self.states = states

    def filter(self, context):
        return filter(lambda state: state.should_apply(context), self.states)
    